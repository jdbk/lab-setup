#! /bin/bash

curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker $USER
sudo apt-get update
sudo apt install docker-compose -y
curl -fsSL https://code-server.dev/install.sh | sh
sudo systemctl enable --now code-server@$USER
sleep 1
sed -i.bak 's/bind-addr: 127.0.0.1:8080/bind-addr: 0.0.0.0:8080/' ~/.config/code-server/config.yaml
sed -i.bak 's/auth: password/auth: none/' ~/.config/code-server/config.yaml
sudo systemctl restart code-server@$USER
sudo docker run -d --restart unless-stopped --dns 10.1.1.4 --dns 10.1.1.5 --dns 10.1.1.6 --name=firefox -p 5800:5800 -v /docker/appdata/firefox:/config:rw --shm-size 2g jlesage/firefox
sudo -- sh -c "echo # k8s api records >> /etc/hosts"
sudo -- sh -c "echo 10.1.1.4 truhlarna-k8s.lab >> /etc/hosts"
sudo -- sh -c "echo 10.1.1.5 truhlarna-k8s.lab >> /etc/hosts"
sudo -- sh -c "echo 10.1.1.6 truhlarna-k8s.lab >> /etc/hosts"
sudo snap install kubectl --classic



